import { Request, Response } from 'express';
import Usuario from '../models/Usuario';

export const getAllUsuarios = async( req: Request , res: Response ) => {
    const usuarios = await Usuario.findAll();
    res.json({usuarios});
}

export const getUsuarioById = async( req: Request , res: Response ) => {
    const { id } = req.params;
    const usuario = await Usuario.findByPk(id);
    if (usuario) {
        res.json( usuario );        
    } else {
        res.status(400).json({
            msg: 'No existe registro de un usuario con ese id'
        });
    }
}

export const createUsuario = async( req: Request , res: Response ) => {
    const { body } = req;
    try {
        const existeEmail = await Usuario.findOne({ where: { correo: body.correo }});
        if (existeEmail) {
            return res.status(400).json({
                msg: "Ya existe un usuario con ese correo"
            });
        };
        const existeCURP = await Usuario.findOne({ where: { correo: body.CURP }});
        if (existeCURP) {
            return res.status(400).json({
                msg: "Ya existe un usuario con ese CURP"
            });
        };
        body.rol = [5,8];
        body.activo = true;
        const newUsuario = await Usuario.create(body);
        res.json({ 
            msg: "Usuario creado con éxito",
            newUsuario 
        });        
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar crear un usuario, contacte al admin.'
        });

    }
}

export const updateUsuario = async( req: Request , res: Response ) => {
    const { id } = req.params;
    const { body } = req;
    try {
        const usuario = await Usuario.findByPk(id);
        if (!usuario) {
            return res.status(400).json({
                msg: "No existe un usuario con ese id"
            });
        };
        const usuarioUpdated = await usuario.update(body);
        res.json({ 
            msg: "Usuario actualizado con éxito",
            usuarioUpdated 
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar actualizar un usuario, contacte al admin.'
        });
    }
}

export const deleteUsuarioById = async( req: Request , res: Response ) => {
    const { id } = req.params;
    try {
        const usuario = await Usuario.findByPk(id);
        if (!usuario) {
            return res.status(400).json({
                msg: "No existe un usuario con ese id"
            });
        };
        const usuarioDeleted = await usuario.update({ activo: false });
        res.json({
            msg: "Usuario eliminado con éxito", 
            usuarioDeleted
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar crear un usuario, contacte al admin.'
        });
    }
}