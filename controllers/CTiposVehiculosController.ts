import { Request, Response } from 'express';
import { Sequelize } from 'sequelize';
import CTipoVehiculo from '../models/CTipoVehiculo';

export const createTipoVehiculo = async(req: Request, res: Response) => {
    try {
        const { body } = req;
        
        const tipoVehiculoMatch = await CTipoVehiculo.findOne({
                where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('tipo_vehiculo')),
                Sequelize.fn('lower', body.tipo_vehiculo))
        });
        if(tipoVehiculoMatch){
            return res.status(400).json({
                msg: "Ya existe el tipo de vehiculo que intenta registrar",
            });
        }else{
            body.activo = true;
            const newTipoVehiculo = await CTipoVehiculo.create(body);
            res.json({
                msg: "Se registró con éxito el nuevo tipo de vehiculo",
                newTipoVehiculo
            });  
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar registrar el nuevo tipo de vehiculo."
        });
    }
};

export const getAllTiposVehiculos = async(req: Request, res: Response) => {
    try {
        const tiposVehiculos = await CTipoVehiculo.findAll();
        if(tiposVehiculos.length == 0){
            res.json({
                msg: "No hay tipos de vehiculo para mostrar"
            });
        }else{
            res.json({
                tiposVehiculos
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar consultar todos los tipos de vehiculo."
        });
    }
};

export const getTipoVehiculoById = async(req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const tipoVehiculoMatch = await CTipoVehiculo.findByPk( id );
        if(!tipoVehiculoMatch){
            res.status(400).json({
                msg: "No existe un tipo de vehiculo con ese id."
            });
        }else{
            res.json({
                tipoVehiculoMatch
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar consultar un tipo de vehiculo por id."
        });
    }
};

export const updateTipoVehiculo = async(req: Request, res: Response) => {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoVehiculoMatch = await CTipoVehiculo.findByPk( id );
        if( !tipoVehiculoMatch ){
            res.status(400).json({
                msg: "No existe el tipo de vehiculo que intenta modificar."
            });
        }else{
            const tipoVehiculoUpdated = await tipoVehiculoMatch.update(body);
            res.json({
                msg: "Se modificó el registro de tipo de vehiculo con éxito.",
                tipoVehiculoUpdated
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar modificar el registro de tipo de vehiculo."
        });
    }
};

export const deleteTipoVehiculo = async(req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const { body } = req;
        const tipoVehiculoMatch = await CTipoVehiculo.findByPk( id );
        if(!tipoVehiculoMatch){
            res.status(400).json({
                msg: "No existe registro del tipo de vehiculo que intenta eliminar."
            });
        }else{
            body.activo = false;
            const tipoVehiculoDeleted = await tipoVehiculoMatch.update(body);
            res.json({
               msg: "Se eliminó el registro de tipo de vehiculo con éxito.",
               tipoVehiculoDeleted 
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar eliminar el registro de tipo de vehiculo."
        });
    }
};