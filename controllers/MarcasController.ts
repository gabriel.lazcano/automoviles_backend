import { Request, Response } from 'express';
import Marcas from '../models/Marcas';
import { Sequelize } from 'sequelize';

export const createMarca = async (req: Request, res: Response) => {
    const { body } = req;
    try {
        const matchMarca = await Marcas.findOne({
            where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('marca')),
                Sequelize.fn('lower', body.marca))
        });
        if (matchMarca) {
            return res.status(400).json({
                msg: "Ya existe una marca con ese nombre."
            });
        };
        body.activo = true;
        const newMarca = await Marcas.create(body);
        res.json({
            msg: "Marca registrada con éxito",
            newMarca
        });
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar registrar la nueva marca, contacte al admin.'
        });
    }
};

export const getAllMarcas = async (req: Request, res: Response) => {
    try {
        const marcas = await Marcas.findAll();
        if (marcas.length === 0) {
            res.json({
                msg: "No hay marcas para mostrar"
            });
        }
        res.json({ marcas });
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            msg: "Algo salió mal al intentar consultar todas las marcas"
        });
    }
};

export const getMarcaById = async(req: Request, res: Response) => {
    try {
        const { id } = req.params;
        const marca = await Marcas.findByPk(id);
        if (!marca) {
            return res.status(400).json({
                msg: "No existe una categoría con ese id"
            });
        } else {
            res.json(marca);            
        }
    } catch (error) {
        console.log(error);
        return res.status(400).json({
            msg: "Algo salió mal al intentar consultar una cateria"
        });
    }
};

export const updateMarca = async (req: Request, res: Response) => {
    try {
        const { body } = req;
        const { id } = req.params;
        const marca = await Marcas.findByPk(id);
        if (!marca) {
            res.status(400).json({
                msg: 'No existe la marca que intenta modificar.'
            });
        } else {
            body.id = id;
            const marcaModificada = await marca.update(body);
            res.json({
                msg: 'Se modificó el registro de la marca con éxito.',
                marcaModificada
            });
        };
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar actualizar la marca, contacte al admin.'
        });
    }
};

export const deleteMarca = async (req: Request, res: Response) => {
    const { body } = req;
    const { id } = req.params;
    try {
        const marca = await Marcas.findByPk(id);
        if (!marca) {
            res.status(400).json({
                msg: 'No existe la marca que intenta eliminar.'
            });
        } else {
            body.id = id;
            body.activo = false;
            const marcaEliminada = await marca.update(body);
            res.json({
                msg: 'Se eliminó el registro de la marca con éxito.',
                marcaEliminada
            });
        };
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar eliminar la marca, contacte al admin.'
        });
    }
};