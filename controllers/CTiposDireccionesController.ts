import { Request, Response } from 'express';
import CTiposDirecciones from '../models/CTipoDireccion';
import { Sequelize } from 'sequelize';

export const createTipoDireccion = async( req: Request, res: Response ) => {
    try {
        const { body } = req;
        const tipoDireccionMatch = await CTiposDirecciones.findOne({
            where: Sequelize.where(Sequelize.fn('lower', Sequelize.col('tipo_direccion')),
                Sequelize.fn('lower', body.tipo_direccion))
        });
        if(tipoDireccionMatch){
            res.json({
                msg: "Ya existe un registro con ese tipo de dirección."
            });
        }else{
            body.activo = true;
            const newTipoDireccion = await CTiposDirecciones.create(body);
            res.json({
                msg: "Se registró con éxito el nuevo tipo de dirección.",
                newTipoDireccion
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar registrar el nuevo tipo de dirección."
        });
    }
}

export const getAllTiposDireccion = async( req: Request, res: Response ) => {
    try {
        const tiposDireccion = await CTiposDirecciones.findAll();
        if( tiposDireccion.length == 0 ){
            res.json({
                msg: "No hay tipos de dirección para mostrar"
            });
        }else{
            res.json({
                tiposDireccion
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar consultar los tipos de dirección"
        });
    }
}

export const getTipoDireccionById = async( req: Request, res: Response ) => {
    try {
        const { id } = req.params;
        const tipoDireccion = await CTiposDirecciones.findByPk(id);
        if( !tipoDireccion ){
            res.json({
                msg: "No existe un tipo de dirección con ese id."
            });
        }else{
            res.json({
                tipoDireccion
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar consultar el tipo de dirección por id."
        });
    }
}


export const updateTipoDireccion = async( req: Request, res: Response ) => {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoDireccionMatch = await CTiposDirecciones.findByPk(id);
        if(tipoDireccionMatch){
            const tipoDireccionUpdated = await tipoDireccionMatch.update(body);
            res.json({
                msg: "Se modificó con éxito el registo de tipo de dirección.",
                tipoDireccionUpdated
            });
        }else{
            res.json({
                msg: "No existe el tipo de dirección que se intenta modificar."
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar actualizar el registro de tipo de dirección."
        });
    }
}

export const deleteTipoDireccion = async( req: Request, res: Response ) => {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoDireccionMatch = await CTiposDirecciones.findByPk(id);
        if(tipoDireccionMatch){
            body.activo = false;
            const tipoDireccionDeleted = await tipoDireccionMatch.update(body);
            res.json({
                msg: "Se eliminó con éxito el registo de tipo de dirección.",
                tipoDireccionDeleted
            });
        }else{
            res.json({
                msg: "No existe el tipo de dirección que se intenta eliminar."
            });
        }
    } catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar eliminar el registro de tipo de dirección."
        });
    }
}