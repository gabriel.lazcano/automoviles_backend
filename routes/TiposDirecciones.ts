import Router from 'express';
import { createTipoDireccion, getAllTiposDireccion, getTipoDireccionById, updateTipoDireccion, deleteTipoDireccion } from '../controllers/CTiposDireccionesController';

const router = Router();

router.post('/', createTipoDireccion);
router.get('/:id', getTipoDireccionById);
router.get('/', getAllTiposDireccion);
router.put('/:id', updateTipoDireccion);
router.delete('/:id', deleteTipoDireccion);

export default router;
