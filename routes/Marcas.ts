import Router from 'express';
import { createMarca, deleteMarca, getAllMarcas, getMarcaById, updateMarca } from '../controllers/MarcasController';
import { check } from 'express-validator';

const router = Router();

router.get('/', getAllMarcas);

router.get('/:id', getMarcaById);

router.post('/', [
    check('marca' ,'La marca es obligatoria').not().isEmpty(),
    check('activo', 'La bandera de estatus debe ser de tipo boolean').not().isBoolean()
], createMarca);

router.put('/:id',[
    check('marca' ,'La marca es obligatoria').not().isEmpty(),
    check('activo', 'La bandera de estatus debe ser de tipo boolean').not().isBoolean()
], updateMarca);

router.delete('/:id', deleteMarca);

export default router;