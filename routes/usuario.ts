import Router from 'express';
import { check } from 'express-validator';
import { validarCampos } from '../middlewares/validar-campos';
import { validarCURP, validarRFC } from '../middlewares/helper';
import { getAllUsuarios, 
         getUsuarioById, 
         createUsuario, 
         updateUsuario, 
         deleteUsuarioById } from '../controllers/UsuarioController';

const router = Router();

router.get('/', getAllUsuarios);

router.get('/:id', getUsuarioById);

router.post('/', 
[
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('nombre', 'El nombre es demasiado extenso').isLength({ max: 200 }),
    check('ap_paterno', 'El apellido paterno es obligatorio').not().isEmpty(),
    check('ap_paterno', 'El apellido paterno es demasiado extenso').isLength({ max: 200 }),
    check('ap_materno', 'El apellido materno es obligatorio').not().isEmpty(),
    check('ap_materno', 'El apellido materno es demasiado extenso').isLength({ max: 200 }),
    check('CURP', 'La CURP es obligatoria').not().isEmpty(),
    check('CURP').custom(validarCURP),
    check('correo', 'El correo ingresado no es válido').isEmail(),
    check('correo', 'El correo es demasiado extenso').isLength({ max: 200 }),
    check('RFC').custom(validarRFC),
    check('usuario', 'El usuario es obligatorio').not().isEmpty(),
    check('usuario', 'El usuario debe tener entre 8 y 20 caracteres').isLength({ min: 8, max: 20 }),
    check('password', 'El password es obligatorio').not().isEmpty(),
    check('password', 'El password debe tener entre 8 y 200 caracteres').isLength({ min: 8, max: 200 }),
    check('direccion', 'La dirección no está formateada correctamente').not().isJSON(),
    validarCampos
]
,createUsuario);

router.put('/:id',  
[
    check('nombre', 'El nombre es obligatorio').not().isEmpty(),
    check('nombre', 'El nombre es demasiado extenso').isLength({ max: 200 }),
    check('ap_paterno', 'El apellido paterno es obligatorio').not().isEmpty(),
    check('ap_paterno', 'El apellido paterno es demasiado extenso').isLength({ max: 200 }),
    check('ap_materno', 'El apellido materno es obligatorio').not().isEmpty(),
    check('ap_materno', 'El apellido materno es demasiado extenso').isLength({ max: 200 }),
    check('CURP', 'La CURP es obligatoria').not().isEmpty(),
    check('CURP').custom(validarCURP),
    check('correo', 'El correo ingresado no es válido').isEmail(),
    check('correo', 'El correo es demasiado extenso').isLength({ max: 200 }),
    check('RFC').custom(validarRFC),
    check('usuario', 'El usuario es obligatorio').not().isEmpty(),
    check('usuario', 'El usuario debe tener entre 8 y 20 caracteres').isLength({ min: 8, max: 20 }),
    check('password', 'El password es obligatorio').not().isEmpty(),
    check('password', 'El password debe tener entre 8 y 200 caracteres').isLength({ min: 8, max: 200 }),
    check('direccion', 'La dirección no está formateada correctamente').not().isJSON(),
    validarCampos
]
,updateUsuario);

router.delete('/:id', deleteUsuarioById);

export default router;