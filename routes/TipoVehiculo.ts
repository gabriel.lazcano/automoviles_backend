import Router from 'express';
import { createTipoVehiculo, 
         getAllTiposVehiculos, 
         getTipoVehiculoById, 
         updateTipoVehiculo, 
         deleteTipoVehiculo } from '../controllers/CTiposVehiculosController';
import { check } from 'express-validator';
import { validarCampos } from '../middlewares/validar-campos';
const router = Router();

router.post('/', [
    check('tipo_vehiculo','El tipo de vehiculo es obligatorio.').not().isEmpty(),
    check('img', 'La imagen de referencia es obligatoria.').not().isEmpty(),
    validarCampos
], createTipoVehiculo);

router.get('/', getAllTiposVehiculos);

router.get('/:id', getTipoVehiculoById);

router.put('/:id',[
    check('tipo_vehiculo','El tipo de vehiculo es obligatorio.').not().isEmpty(),
    check('img', 'La imagen de referencia es obligatoria.').not().isEmpty(),
    validarCampos
], updateTipoVehiculo);

router.delete('/:id', 
[
    check('tipo_vehiculo','No es necesario agregar el tipo de vehiculo para ésta acción.').isEmpty(),
    check('img', 'No es necesario agregar la imagen de referencia para ésta acción.').isEmpty(),
    validarCampos
],deleteTipoVehiculo);

export default router;