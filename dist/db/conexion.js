"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
dotenv_1.default.config();
const sequelize_1 = require("sequelize");
const database = process.env.DATABASE ? process.env.DATABASE : "";
const user_db = process.env.USER_DB ? process.env.USER_DB : "";
const pwd_db = process.env.PWD_DB ? process.env.PWD_DB : "";
const host_db = process.env.HOST_DB ? process.env.HOST_DB : "";
const port_db = process.env.PORT_DB ? +process.env.PORT_DB : 0;
const db = new sequelize_1.Sequelize(database, user_db, pwd_db, {
    host: host_db,
    port: port_db,
    dialect: 'postgres'
});
exports.default = db;
//# sourceMappingURL=conexion.js.map