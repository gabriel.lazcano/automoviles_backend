"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const sequelize_1 = require("sequelize");
const conexion_1 = __importDefault(require("../db/conexion"));
const Usuario = conexion_1.default.define('usuarios', {
    nombre: {
        type: sequelize_1.DataTypes.STRING
    },
    ap_paterno: {
        type: sequelize_1.DataTypes.STRING
    },
    ap_materno: {
        type: sequelize_1.DataTypes.STRING
    },
    CURP: {
        type: sequelize_1.DataTypes.STRING
    },
    RFC: {
        type: sequelize_1.DataTypes.STRING
    },
    direccion: {
        type: sequelize_1.DataTypes.JSON
    },
    correo: {
        type: sequelize_1.DataTypes.STRING
    },
    rol: {
        type: sequelize_1.DataTypes.ARRAY(sequelize_1.INTEGER)
    },
    activo: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    usuario: {
        type: sequelize_1.DataTypes.STRING
    },
    password: {
        type: sequelize_1.DataTypes.STRING
    },
    google: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    createdAt: {
        type: sequelize_1.DATE
    },
    updatedAt: {
        type: sequelize_1.DATE
    }
});
exports.default = Usuario;
//# sourceMappingURL=Usuario.js.map