"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const conexion_1 = __importDefault(require("../db/conexion"));
const sequelize_1 = require("sequelize");
const CTipoVehiculo = conexion_1.default.define('c_tipo_vehiculo', {
    tipo_vehiculo: {
        type: sequelize_1.DataTypes.STRING
    },
    img: {
        type: sequelize_1.DataTypes.STRING
    },
    activo: {
        type: sequelize_1.DataTypes.BOOLEAN
    },
    createdAt: {
        type: sequelize_1.DATE
    },
    updatedAt: {
        type: sequelize_1.DATE
    }
}, {
    freezeTableName: true
});
exports.default = CTipoVehiculo;
//# sourceMappingURL=CTipoVehiculo.js.map