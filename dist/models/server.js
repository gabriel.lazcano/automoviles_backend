"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const Usuario_1 = __importDefault(require("../routes/Usuario"));
const Marcas_1 = __importDefault(require("../routes/Marcas"));
const TiposDirecciones_1 = __importDefault(require("../routes/TiposDirecciones"));
const TipoVehiculo_1 = __importDefault(require("../routes/TipoVehiculo"));
const cors_1 = __importDefault(require("cors"));
const conexion_1 = __importDefault(require("../db/conexion"));
class Server {
    constructor() {
        this.paths = {
            usuarios: '/api/usuarios',
            marcas: '/api/marcas',
            tipoDireccion: '/api/tipo_direccion',
            tipoVehiculo: '/api/tipo_vehiculo'
        };
        this.app = (0, express_1.default)();
        this.port = process.env.PORT || "8084";
        this.conexionBD();
        this.middlewares();
        this.routes();
    }
    conexionBD() {
        return __awaiter(this, void 0, void 0, function* () {
            try {
                yield conexion_1.default.authenticate();
                console.log('DATABASE ONLINE');
            }
            catch (error) {
                console.log(error);
                throw new Error("Error: Algo salió mal al intentar conectarse a la BD.");
            }
        });
    }
    middlewares() {
        this.app.use((0, cors_1.default)());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.static('public'));
    }
    routes() {
        this.app.use(this.paths.usuarios, Usuario_1.default);
        this.app.use(this.paths.marcas, Marcas_1.default);
        this.app.use(this.paths.tipoDireccion, TiposDirecciones_1.default);
        this.app.use(this.paths.tipoVehiculo, TipoVehiculo_1.default);
    }
    listen() {
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto ${this.port}`);
        });
    }
}
exports.default = Server;
//# sourceMappingURL=Server.js.map