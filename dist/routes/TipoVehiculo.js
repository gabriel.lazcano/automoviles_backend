"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const CTiposVehiculosController_1 = require("../controllers/CTiposVehiculosController");
const express_validator_1 = require("express-validator");
const validar_campos_1 = require("../middlewares/validar-campos");
const router = (0, express_1.default)();
router.post('/', [
    (0, express_validator_1.check)('tipo_vehiculo', 'El tipo de vehiculo es obligatorio.').not().isEmpty(),
    (0, express_validator_1.check)('img', 'La imagen de referencia es obligatoria.').not().isEmpty(),
    validar_campos_1.validarCampos
], CTiposVehiculosController_1.createTipoVehiculo);
router.get('/', CTiposVehiculosController_1.getAllTiposVehiculos);
router.get('/:id', CTiposVehiculosController_1.getTipoVehiculoById);
router.put('/:id', [
    (0, express_validator_1.check)('tipo_vehiculo', 'El tipo de vehiculo es obligatorio.').not().isEmpty(),
    (0, express_validator_1.check)('img', 'La imagen de referencia es obligatoria.').not().isEmpty(),
    validar_campos_1.validarCampos
], CTiposVehiculosController_1.updateTipoVehiculo);
router.delete('/:id', [
    (0, express_validator_1.check)('tipo_vehiculo', 'No es necesario agregar el tipo de vehiculo para ésta acción.').isEmpty(),
    (0, express_validator_1.check)('img', 'No es necesario agregar la imagen de referencia para ésta acción.').isEmpty(),
    validar_campos_1.validarCampos
], CTiposVehiculosController_1.deleteTipoVehiculo);
exports.default = router;
//# sourceMappingURL=TipoVehiculo.js.map