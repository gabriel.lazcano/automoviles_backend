"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const CTiposDireccionesController_1 = require("../controllers/CTiposDireccionesController");
const router = (0, express_1.default)();
router.post('/', CTiposDireccionesController_1.createTipoDireccion);
router.get('/:id', CTiposDireccionesController_1.getTipoDireccionById);
router.get('/', CTiposDireccionesController_1.getAllTiposDireccion);
router.put('/:id', CTiposDireccionesController_1.updateTipoDireccion);
router.delete('/:id', CTiposDireccionesController_1.deleteTipoDireccion);
exports.default = router;
//# sourceMappingURL=TiposDirecciones.js.map