"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const MarcasController_1 = require("../controllers/MarcasController");
const express_validator_1 = require("express-validator");
const router = (0, express_1.default)();
router.get('/', MarcasController_1.getAllMarcas);
router.get('/:id', MarcasController_1.getMarcaById);
router.post('/', [
    (0, express_validator_1.check)('marca', 'La marca es obligatoria').not().isEmpty(),
    (0, express_validator_1.check)('activo', 'La bandera de estatus debe ser de tipo boolean').not().isBoolean()
], MarcasController_1.createMarca);
router.put('/:id', [
    (0, express_validator_1.check)('marca', 'La marca es obligatoria').not().isEmpty(),
    (0, express_validator_1.check)('activo', 'La bandera de estatus debe ser de tipo boolean').not().isBoolean()
], MarcasController_1.updateMarca);
router.delete('/:id', MarcasController_1.deleteMarca);
exports.default = router;
//# sourceMappingURL=Marcas.js.map