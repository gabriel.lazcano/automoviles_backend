"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const express_validator_1 = require("express-validator");
const validar_campos_1 = require("../middlewares/validar-campos");
const helper_1 = require("../middlewares/helper");
const UsuarioController_1 = require("../controllers/UsuarioController");
const router = (0, express_1.default)();
router.get('/', UsuarioController_1.getAllUsuarios);
router.get('/:id', UsuarioController_1.getUsuarioById);
router.post('/', [
    (0, express_validator_1.check)('nombre', 'El nombre es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('nombre', 'El nombre es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('ap_paterno', 'El apellido paterno es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('ap_paterno', 'El apellido paterno es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('ap_materno', 'El apellido materno es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('ap_materno', 'El apellido materno es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('CURP', 'La CURP es obligatoria').not().isEmpty(),
    (0, express_validator_1.check)('CURP').custom(helper_1.validarCURP),
    (0, express_validator_1.check)('correo', 'El correo ingresado no es válido').isEmail(),
    (0, express_validator_1.check)('correo', 'El correo es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('RFC').custom(helper_1.validarRFC),
    (0, express_validator_1.check)('usuario', 'El usuario es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('usuario', 'El usuario debe tener entre 8 y 20 caracteres').isLength({ min: 8, max: 20 }),
    (0, express_validator_1.check)('password', 'El password es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('password', 'El password debe tener entre 8 y 200 caracteres').isLength({ min: 8, max: 200 }),
    (0, express_validator_1.check)('direccion', 'La dirección no está formateada correctamente').not().isJSON(),
    validar_campos_1.validarCampos
], UsuarioController_1.createUsuario);
router.put('/:id', [
    (0, express_validator_1.check)('nombre', 'El nombre es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('nombre', 'El nombre es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('ap_paterno', 'El apellido paterno es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('ap_paterno', 'El apellido paterno es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('ap_materno', 'El apellido materno es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('ap_materno', 'El apellido materno es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('CURP', 'La CURP es obligatoria').not().isEmpty(),
    (0, express_validator_1.check)('CURP').custom(helper_1.validarCURP),
    (0, express_validator_1.check)('correo', 'El correo ingresado no es válido').isEmail(),
    (0, express_validator_1.check)('correo', 'El correo es demasiado extenso').isLength({ max: 200 }),
    (0, express_validator_1.check)('RFC').custom(helper_1.validarRFC),
    (0, express_validator_1.check)('usuario', 'El usuario es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('usuario', 'El usuario debe tener entre 8 y 20 caracteres').isLength({ min: 8, max: 20 }),
    (0, express_validator_1.check)('password', 'El password es obligatorio').not().isEmpty(),
    (0, express_validator_1.check)('password', 'El password debe tener entre 8 y 200 caracteres').isLength({ min: 8, max: 200 }),
    (0, express_validator_1.check)('direccion', 'La dirección no está formateada correctamente').not().isJSON(),
    validar_campos_1.validarCampos
], UsuarioController_1.updateUsuario);
router.delete('/:id', UsuarioController_1.deleteUsuarioById);
exports.default = router;
//# sourceMappingURL=Usuario.js.map