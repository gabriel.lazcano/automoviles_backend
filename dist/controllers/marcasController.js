"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteMarca = exports.updateMarca = exports.getMarcaById = exports.getAllMarcas = exports.createMarca = void 0;
const Marcas_1 = __importDefault(require("../models/Marcas"));
const sequelize_1 = require("sequelize");
const createMarca = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const matchMarca = yield Marcas_1.default.findOne({
            where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('marca')), sequelize_1.Sequelize.fn('lower', body.marca))
        });
        if (matchMarca) {
            return res.status(400).json({
                msg: "Ya existe una marca con ese nombre."
            });
        }
        ;
        body.activo = true;
        const newMarca = yield Marcas_1.default.create(body);
        res.json({
            msg: "Marca registrada con éxito",
            newMarca
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar registrar la nueva marca, contacte al admin.'
        });
    }
});
exports.createMarca = createMarca;
const getAllMarcas = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const marcas = yield Marcas_1.default.findAll();
        if (marcas.length === 0) {
            res.json({
                msg: "No hay marcas para mostrar"
            });
        }
        res.json({ marcas });
    }
    catch (error) {
        console.log(error);
        return res.status(400).json({
            msg: "Algo salió mal al intentar consultar todas las marcas"
        });
    }
});
exports.getAllMarcas = getAllMarcas;
const getMarcaById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const marca = yield Marcas_1.default.findByPk(id);
        if (!marca) {
            return res.status(400).json({
                msg: "No existe una categoría con ese id"
            });
        }
        else {
            res.json(marca);
        }
    }
    catch (error) {
        console.log(error);
        return res.status(400).json({
            msg: "Algo salió mal al intentar consultar una cateria"
        });
    }
});
exports.getMarcaById = getMarcaById;
const updateMarca = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const { id } = req.params;
        const marca = yield Marcas_1.default.findByPk(id);
        if (!marca) {
            res.status(400).json({
                msg: 'No existe la marca que intenta modificar.'
            });
        }
        else {
            body.id = id;
            const marcaModificada = yield marca.update(body);
            res.json({
                msg: 'Se modificó el registro de la marca con éxito.',
                marcaModificada
            });
        }
        ;
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar actualizar la marca, contacte al admin.'
        });
    }
});
exports.updateMarca = updateMarca;
const deleteMarca = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    const { id } = req.params;
    try {
        const marca = yield Marcas_1.default.findByPk(id);
        if (!marca) {
            res.status(400).json({
                msg: 'No existe la marca que intenta eliminar.'
            });
        }
        else {
            body.id = id;
            body.activo = false;
            const marcaEliminada = yield marca.update(body);
            res.json({
                msg: 'Se eliminó el registro de la marca con éxito.',
                marcaEliminada
            });
        }
        ;
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar eliminar la marca, contacte al admin.'
        });
    }
});
exports.deleteMarca = deleteMarca;
//# sourceMappingURL=MarcasController.js.map