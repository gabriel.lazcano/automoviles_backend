"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTipoVehiculo = exports.updateTipoVehiculo = exports.getTipoVehiculoById = exports.getAllTiposVehiculos = exports.createTipoVehiculo = void 0;
const sequelize_1 = require("sequelize");
const CTipoVehiculo_1 = __importDefault(require("../models/CTipoVehiculo"));
const createTipoVehiculo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const tipoVehiculoMatch = yield CTipoVehiculo_1.default.findOne({
            where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('tipo_vehiculo')), sequelize_1.Sequelize.fn('lower', body.tipo_vehiculo))
        });
        if (tipoVehiculoMatch) {
            return res.status(400).json({
                msg: "Ya existe el tipo de vehiculo que intenta registrar",
            });
        }
        else {
            body.activo = true;
            const newTipoVehiculo = yield CTipoVehiculo_1.default.create(body);
            res.json({
                msg: "Se registró con éxito el nuevo tipo de vehiculo",
                newTipoVehiculo
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar registrar el nuevo tipo de vehiculo."
        });
    }
});
exports.createTipoVehiculo = createTipoVehiculo;
const getAllTiposVehiculos = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const tiposVehiculos = yield CTipoVehiculo_1.default.findAll();
        if (tiposVehiculos.length == 0) {
            res.json({
                msg: "No hay tipos de vehiculo para mostrar"
            });
        }
        else {
            res.json({
                tiposVehiculos
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar consultar todos los tipos de vehiculo."
        });
    }
});
exports.getAllTiposVehiculos = getAllTiposVehiculos;
const getTipoVehiculoById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const tipoVehiculoMatch = yield CTipoVehiculo_1.default.findByPk(id);
        if (!tipoVehiculoMatch) {
            res.status(400).json({
                msg: "No existe un tipo de vehiculo con ese id."
            });
        }
        else {
            res.json({
                tipoVehiculoMatch
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar consultar un tipo de vehiculo por id."
        });
    }
});
exports.getTipoVehiculoById = getTipoVehiculoById;
const updateTipoVehiculo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoVehiculoMatch = yield CTipoVehiculo_1.default.findByPk(id);
        if (!tipoVehiculoMatch) {
            res.status(400).json({
                msg: "No existe el tipo de vehiculo que intenta modificar."
            });
        }
        else {
            const tipoVehiculoUpdated = yield tipoVehiculoMatch.update(body);
            res.json({
                msg: "Se modificó el registro de tipo de vehiculo con éxito.",
                tipoVehiculoUpdated
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar modificar el registro de tipo de vehiculo."
        });
    }
});
exports.updateTipoVehiculo = updateTipoVehiculo;
const deleteTipoVehiculo = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const { body } = req;
        const tipoVehiculoMatch = yield CTipoVehiculo_1.default.findByPk(id);
        if (!tipoVehiculoMatch) {
            res.status(400).json({
                msg: "No existe registro del tipo de vehiculo que intenta eliminar."
            });
        }
        else {
            body.activo = false;
            const tipoVehiculoDeleted = yield tipoVehiculoMatch.update(body);
            res.json({
                msg: "Se eliminó el registro de tipo de vehiculo con éxito.",
                tipoVehiculoDeleted
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Ocurrió un error al intentar eliminar el registro de tipo de vehiculo."
        });
    }
});
exports.deleteTipoVehiculo = deleteTipoVehiculo;
//# sourceMappingURL=CTiposVehiculosController.js.map