"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteUsuarioById = exports.updateUsuario = exports.createUsuario = exports.getUsuarioById = exports.getAllUsuarios = void 0;
const Usuario_1 = __importDefault(require("../models/Usuario"));
const getAllUsuarios = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const usuarios = yield Usuario_1.default.findAll();
    res.json({ usuarios });
});
exports.getAllUsuarios = getAllUsuarios;
const getUsuarioById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const usuario = yield Usuario_1.default.findByPk(id);
    if (usuario) {
        res.json(usuario);
    }
    else {
        res.status(400).json({
            msg: 'No existe registro de un usuario con ese id'
        });
    }
});
exports.getUsuarioById = getUsuarioById;
const createUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { body } = req;
    try {
        const existeEmail = yield Usuario_1.default.findOne({ where: { correo: body.correo } });
        if (existeEmail) {
            return res.status(400).json({
                msg: "Ya existe un usuario con ese correo"
            });
        }
        ;
        const existeCURP = yield Usuario_1.default.findOne({ where: { correo: body.CURP } });
        if (existeCURP) {
            return res.status(400).json({
                msg: "Ya existe un usuario con ese CURP"
            });
        }
        ;
        body.rol = [5, 8];
        body.activo = true;
        const newUsuario = yield Usuario_1.default.create(body);
        res.json({
            msg: "Usuario creado con éxito",
            newUsuario
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar crear un usuario, contacte al admin.'
        });
    }
});
exports.createUsuario = createUsuario;
const updateUsuario = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    const { body } = req;
    try {
        const usuario = yield Usuario_1.default.findByPk(id);
        if (!usuario) {
            return res.status(400).json({
                msg: "No existe un usuario con ese id"
            });
        }
        ;
        const usuarioUpdated = yield usuario.update(body);
        res.json({
            msg: "Usuario actualizado con éxito",
            usuarioUpdated
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar actualizar un usuario, contacte al admin.'
        });
    }
});
exports.updateUsuario = updateUsuario;
const deleteUsuarioById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id } = req.params;
    try {
        const usuario = yield Usuario_1.default.findByPk(id);
        if (!usuario) {
            return res.status(400).json({
                msg: "No existe un usuario con ese id"
            });
        }
        ;
        const usuarioDeleted = yield usuario.update({ activo: false });
        res.json({
            msg: "Usuario eliminado con éxito",
            usuarioDeleted
        });
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: 'Error al intentar crear un usuario, contacte al admin.'
        });
    }
});
exports.deleteUsuarioById = deleteUsuarioById;
//# sourceMappingURL=UsuarioController.js.map