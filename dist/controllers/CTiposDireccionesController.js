"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.deleteTipoDireccion = exports.updateTipoDireccion = exports.getTipoDireccionById = exports.getAllTiposDireccion = exports.createTipoDireccion = void 0;
const CTipoDireccion_1 = __importDefault(require("../models/CTipoDireccion"));
const sequelize_1 = require("sequelize");
const createTipoDireccion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const tipoDireccionMatch = yield CTipoDireccion_1.default.findOne({
            where: sequelize_1.Sequelize.where(sequelize_1.Sequelize.fn('lower', sequelize_1.Sequelize.col('tipo_direccion')), sequelize_1.Sequelize.fn('lower', body.tipo_direccion))
        });
        if (tipoDireccionMatch) {
            res.json({
                msg: "Ya existe un registro con ese tipo de dirección."
            });
        }
        else {
            body.activo = true;
            const newTipoDireccion = yield CTipoDireccion_1.default.create(body);
            res.json({
                msg: "Se registró con éxito el nuevo tipo de dirección.",
                newTipoDireccion
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar registrar el nuevo tipo de dirección."
        });
    }
});
exports.createTipoDireccion = createTipoDireccion;
const getAllTiposDireccion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const tiposDireccion = yield CTipoDireccion_1.default.findAll();
        if (tiposDireccion.length == 0) {
            res.json({
                msg: "No hay tipos de dirección para mostrar"
            });
        }
        else {
            res.json({
                tiposDireccion
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar consultar los tipos de dirección"
        });
    }
});
exports.getAllTiposDireccion = getAllTiposDireccion;
const getTipoDireccionById = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { id } = req.params;
        const tipoDireccion = yield CTipoDireccion_1.default.findByPk(id);
        if (!tipoDireccion) {
            res.json({
                msg: "No existe un tipo de dirección con ese id."
            });
        }
        else {
            res.json({
                tipoDireccion
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar consultar el tipo de dirección por id."
        });
    }
});
exports.getTipoDireccionById = getTipoDireccionById;
const updateTipoDireccion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoDireccionMatch = yield CTipoDireccion_1.default.findByPk(id);
        if (tipoDireccionMatch) {
            const tipoDireccionUpdated = yield tipoDireccionMatch.update(body);
            res.json({
                msg: "Se modificó con éxito el registo de tipo de dirección.",
                tipoDireccionUpdated
            });
        }
        else {
            res.json({
                msg: "No existe el tipo de dirección que se intenta modificar."
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar actualizar el registro de tipo de dirección."
        });
    }
});
exports.updateTipoDireccion = updateTipoDireccion;
const deleteTipoDireccion = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const { body } = req;
        const { id } = req.params;
        const tipoDireccionMatch = yield CTipoDireccion_1.default.findByPk(id);
        if (tipoDireccionMatch) {
            body.activo = false;
            const tipoDireccionDeleted = yield tipoDireccionMatch.update(body);
            res.json({
                msg: "Se eliminó con éxito el registo de tipo de dirección.",
                tipoDireccionDeleted
            });
        }
        else {
            res.json({
                msg: "No existe el tipo de dirección que se intenta eliminar."
            });
        }
    }
    catch (error) {
        console.log(error);
        res.status(500).json({
            msg: "Algo salió mal al intentar eliminar el registro de tipo de dirección."
        });
    }
});
exports.deleteTipoDireccion = deleteTipoDireccion;
//# sourceMappingURL=CTiposDireccionesController.js.map