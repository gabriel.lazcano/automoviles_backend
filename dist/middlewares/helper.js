"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.validarRFC = exports.validarCURP = void 0;
const validarCURP = (CURP) => {
    var regexCURP = /^([A-Z&]{1})([AEIOU]{1})([A-Z&]{1})([A-Z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([HM]{1})([AS|BC|BS|CC|CS|CH|CL|CM|DF|DG|GT|GR|HG|JC|MC|MN|MS|NT|NL|OC|PL|QT|QR|SP|SL|SR|TC|TS|TL|VZ|YN|ZS|NE]{2})([^A|E|I|O|U]{1})([^A|E|I|O|U]{1})([^A|E|I|O|U]{1})([0-9A-Z]{2})$/g;
    var validado = CURP.match(regexCURP);
    console.log("CURP", validado);
    if (!validado) {
        throw new Error('La CURP ingresada no cumple con el formato.');
    }
    return true;
};
exports.validarCURP = validarCURP;
const validarRFC = (RFC) => {
    var regexRFC = /^([A-Z&]{1})([AEIOU]{1})([A-Z&]{1})([A-Z&]{1})([0-9]{2})(0[1-9]|1[0-2])(0[1-9]|1[0-9]|2[0-9]|3[0-1])([0-9A-Z]{2}|[0-9A-Z]{3})$/g;
    var validado = RFC.match(regexRFC);
    console.log("RFC", validado);
    if (!validado) {
        throw new Error('El RFC ingresado no cumple con el formato.');
    }
    return true;
};
exports.validarRFC = validarRFC;
//# sourceMappingURL=helper.js.map