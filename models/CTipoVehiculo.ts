import db from '../db/conexion';
import { DataTypes, DATE } from 'sequelize';

const CTipoVehiculo = db.define('c_tipo_vehiculo', {
    tipo_vehiculo: {
        type: DataTypes.STRING
    },
    img: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.BOOLEAN
    },
    createdAt: {
        type: DATE
    },
    updatedAt: {
        type: DATE
    }
},{
    freezeTableName: true
});

export default CTipoVehiculo;