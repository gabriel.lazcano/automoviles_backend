import { DataTypes, DATE } from 'sequelize';
import db from '../db/conexion';

const CTiposDirecciones = db.define('c_tipo_direccion', {
    tipo_direccion: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.BOOLEAN
    },
    createdAt: {
        type: DATE
    },
    updatedAt: {
        type: DATE
    }
},{
    freezeTableName: true
});

export default CTiposDirecciones;