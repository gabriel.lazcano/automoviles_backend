import express, { Application } from "express";
import userRoutes from '../routes/Usuario';
import marcasRoutes from "../routes/Marcas";
import tiposDireccionRoutes from "../routes/TiposDirecciones";
import tipoVehiculoRoutes from "../routes/TipoVehiculo";
import cors from 'cors';
import db from '../db/conexion';

class Server {
    private app: Application;
    private port: string;
    private paths = {
        usuarios: '/api/usuarios',
        marcas: '/api/marcas',
        tipoDireccion: '/api/tipo_direccion',
        tipoVehiculo: '/api/tipo_vehiculo'
    };

    constructor(){
        this.app = express();
        this.port = process.env.PORT || "8084";
        this.conexionBD();
        this.middlewares();
        this.routes();
    }

    async conexionBD(){
        try {
            await db.authenticate();
            console.log('DATABASE ONLINE');            
        } catch (error) {
            console.log(error);
            throw new Error("Error: Algo salió mal al intentar conectarse a la BD.");    
        }       
    }

    middlewares(){
        this.app.use( cors() );
        this.app.use( express.json() );
        this.app.use(express.static('public'));
    }

    routes(){
        this.app.use(this.paths.usuarios, userRoutes);
        this.app.use(this.paths.marcas, marcasRoutes);
        this.app.use(this.paths.tipoDireccion, tiposDireccionRoutes);
        this.app.use(this.paths.tipoVehiculo, tipoVehiculoRoutes);
    }

    listen(){
        this.app.listen(this.port, () => {
            console.log(`Servidor corriendo en el puerto ${ this.port }`);
        });
    }
}

export default Server;