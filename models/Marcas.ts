import { DataTypes, DATE } from 'sequelize';
import db from '../db/conexion';

const Marcas = db.define('c_marcas', {
    marca: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.BOOLEAN
    },
    createdAt: {
        type: DATE
    },
    updatedAt: {
        type: DATE
    }
});

export default Marcas;