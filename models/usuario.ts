import { DATE, DataTypes, INTEGER } from 'sequelize';
import db from '../db/conexion';

const Usuario = db.define('usuarios', {
    nombre: {
        type: DataTypes.STRING
    },
    ap_paterno: {
        type: DataTypes.STRING
    },
    ap_materno: {
        type: DataTypes.STRING
    },
    CURP: {
        type: DataTypes.STRING
    },
    RFC: {
        type: DataTypes.STRING
    },
    direccion: {
        type: DataTypes.JSON
    },
    correo: {
        type: DataTypes.STRING
    },
    rol: {
        type: DataTypes.ARRAY(INTEGER)
    },
    activo: {
        type: DataTypes.BOOLEAN
    },
    usuario: {
        type: DataTypes.STRING
    },
    password: {
        type: DataTypes.STRING
    },
    google: {
        type: DataTypes.BOOLEAN
    },
    createdAt: {
        type: DATE
    },
    updatedAt: {
        type: DATE
    }
});

export default Usuario;