import dotenv from 'dotenv';
dotenv.config();

import { Sequelize } from 'sequelize';

const database:string = process.env.DATABASE? process.env.DATABASE : "";
const user_db:string = process.env.USER_DB ? process.env.USER_DB : "";
const pwd_db:string = process.env.PWD_DB ? process.env.PWD_DB : "";
const host_db:string = process.env.HOST_DB ? process.env.HOST_DB : "";
const port_db:number = process.env.PORT_DB ? +process.env.PORT_DB : 0;

const db = new Sequelize( database, user_db, pwd_db, {
    host: host_db,
    port: port_db,
    dialect: 'postgres'
});

export default db;